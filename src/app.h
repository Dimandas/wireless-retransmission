﻿#pragma once

#include <wx/app.h>

namespace DetectPositioningWiFi
{
	class ProjectApp : public wxApp
	{
	public:
		virtual bool OnInit();
	};
}