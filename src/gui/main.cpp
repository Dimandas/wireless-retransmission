﻿///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "main.h"

#include "../sniff/sniff.h"


///////////////////////////////////////////////////////////////////////////

#ifdef WIN32
BOOL LoadNpcapDlls();
#endif

MainFrame::MainFrame(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent, id, title, pos, size, style)
{
	this->SetSizeHints(wxDefaultSize, wxDefaultSize);
	this->SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENU));

	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer2;
	bSizer2 = new wxBoxSizer(wxVERTICAL);

	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer(wxHORIZONTAL);


	bSizer6->Add(0, 0, 1, wxEXPAND, 5);

	m_staticText1 = new wxStaticText(this, wxID_ANY, wxT("Ретранслировать на порт: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText1->Wrap(-1);
	bSizer6->Add(m_staticText1, 0, wxALL, 5);

	m_retrPort = new wxTextCtrl(this, wxID_ANY, wxT("2480"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer6->Add(m_retrPort, 0, wxALL, 5);


	bSizer6->Add(0, 0, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer6, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer(wxHORIZONTAL);


	bSizer7->Add(0, 0, 1, wxEXPAND, 5);

	m_staticText2 = new wxStaticText(this, wxID_ANY, wxT("Фильтровать пакеты UDP по порту назначения: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText2->Wrap(-1);
	bSizer7->Add(m_staticText2, 0, wxALL, 5);

	m_filtrPort = new wxTextCtrl(this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0);
	bSizer7->Add(m_filtrPort, 0, wxALL, 5);


	bSizer7->Add(0, 0, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer7, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer81;
	bSizer81 = new wxBoxSizer(wxHORIZONTAL);


	bSizer81->Add(0, 0, 1, wxEXPAND, 5);

	m_logEnable = new wxCheckBox(this, wxID_ANY, wxT("Логирование (сильно снижается производительность)"), wxDefaultPosition, wxDefaultSize, 0);
	m_logEnable->SetValue(true);
	bSizer81->Add(m_logEnable, 0, wxALL, 5);


	bSizer81->Add(0, 0, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer81, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer(wxHORIZONTAL);


	bSizer71->Add(0, 0, 1, wxEXPAND, 5);

	m_staticText3 = new wxStaticText(this, wxID_ANY, wxT("Адаптер: "), wxDefaultPosition, wxDefaultSize, 0);
	m_staticText3->Wrap(-1);
	bSizer71->Add(m_staticText3, 0, wxALL, 5);

	wxArrayString m_adapterListChoices;
	m_adapterList = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxSize(400, -1), m_adapterListChoices, 0);
	m_adapterList->SetSelection(0);
	bSizer71->Add(m_adapterList, 0, wxALL, 5);

	m_update = new wxButton(this, wxID_ANY, wxT("Обновить"), wxDefaultPosition, wxSize(-1, 30), 0);
	bSizer71->Add(m_update, 0, wxALL, 5);


	bSizer71->Add(0, 0, 1, wxEXPAND, 5);


	bSizer2->Add(bSizer71, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer(wxHORIZONTAL);

	m_start = new wxButton(this, wxID_ANY, wxT("Запустить"), wxDefaultPosition, wxSize(400, -1), 0);
	bSizer8->Add(m_start, 0, wxALIGN_BOTTOM | wxALL, 5);


	bSizer8->Add(0, 0, 1, wxEXPAND, 5);

	m_stop = new wxButton(this, wxID_ANY, wxT("Остановить"), wxDefaultPosition, wxSize(140, -1), 0);
	m_stop->Enable(false);

	bSizer8->Add(m_stop, 0, wxALIGN_BOTTOM | wxALL, 5);


	bSizer2->Add(bSizer8, 1, wxEXPAND, 5);


	bSizer1->Add(bSizer2, 1, wxEXPAND, 5);

	wxBoxSizer* bSizer3;
	bSizer3 = new wxBoxSizer(wxVERTICAL);

	m_logs = new wxRichTextCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY | wxVSCROLL | wxHSCROLL | wxNO_BORDER | wxWANTS_CHARS);
	bSizer3->Add(m_logs, 1, wxEXPAND | wxALL, 5);


	bSizer1->Add(bSizer3, 1, wxEXPAND, 5);


	this->SetSizer(bSizer1);
	this->Layout();

	this->Centre(wxBOTH);

	// Connect Events
	this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MainFrame::MainFrameOnClose));
	this->Connect(wxEVT_HIBERNATE, wxActivateEventHandler(MainFrame::MainFrameOnHibernate));
	m_update->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_updateOnButtonClick), NULL, this);
	m_start->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_startOnButtonClick), NULL, this);
	m_stop->Connect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_stopOnButtonClick), NULL, this);

	// add user code
#ifdef WIN32
	LoadNpcapDlls();
#endif

	m_updateAdapterList();
}

MainFrame::~MainFrame()
{
	// Disconnect Events
	this->Disconnect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(MainFrame::MainFrameOnClose));
	this->Disconnect(wxEVT_HIBERNATE, wxActivateEventHandler(MainFrame::MainFrameOnHibernate));
	m_update->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_updateOnButtonClick), NULL, this);
	m_start->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_startOnButtonClick), NULL, this);
	m_stop->Disconnect(wxEVT_COMMAND_BUTTON_CLICKED, wxCommandEventHandler(MainFrame::m_stopOnButtonClick), NULL, this);
}

void MainFrame::m_startOnButtonClick(wxCommandEvent& event)
{
	m_logs->Clear();

	int selectNumber = m_adapterList->GetSelection();

	if (selectNumber == wxNOT_FOUND)
	{
		return;
	}

	//
	m_start->Enable(false);
	m_stop->Enable(true);
	m_update->Enable(false);
	//
	m_retrPort->Enable(false);
	m_filtrPort->Enable(false);
	m_logEnable->Enable(false);
	m_adapterList->Enable(false);
	//

	wxIPV4address addrLocal;
	addrLocal.AnyAddress();
	wxDatagramSocket sock2tx(addrLocal);

	std::vector<Tins::NetworkInterface> tempList = Tins::NetworkInterface::all();

	wifi_mon monitor;

	Tins::SnifferConfiguration config;

	config.set_pcap_sniffing_method(pcap_dispatch);
	config.set_immediate_mode(true);
	config.set_timeout(50);
	config.set_promisc_mode(true);
	config.set_rfmon(true);

	std::string filtrPort = (std::string)m_filtrPort->GetLineText(0);
	if (filtrPort != "0")
	{
		config.set_filter("udp dst port " + filtrPort);
	}

	m_runThread = true;

	try
	{
		// Sniff on the provided interface in promiscuous mode
		Tins::Sniffer sniffer(tempList[selectNumber].name(), config);

		// Only capture mon packets
		monitor.run(sniffer, m_logs, &sock2tx, (std::string)m_retrPort->GetLineText(0), m_logEnable->IsChecked(), &m_stopThread);
	}
	catch (...)
	{
	}

	//
	m_start->Enable(true);
	m_stop->Enable(false);
	m_update->Enable(true);
	//
	m_retrPort->Enable(true);
	m_filtrPort->Enable(true);
	m_logEnable->Enable(true);
	m_adapterList->Enable(true);
	//

	m_runThread = false;
	m_stopThread = false;

	if (m_exit == true)
	{
		this->Close();
	}

	event.Skip();
}

void MainFrame::m_stopOnButtonClick(wxCommandEvent& event)
{
	m_stopThread = true;

	event.Skip();
}

void MainFrame::MainFrameOnClose(wxCloseEvent& event)
{
	if (m_runThread == false)
	{
		event.Skip();
	}

	m_stopThread = true;
	m_exit = true;    // костыль, нужен для "безопасной" остановки потока с вычисл.
}

void MainFrame::m_updateAdapterList()
{
	std::vector<Tins::NetworkInterface> tempList = Tins::NetworkInterface::all();

	m_adapterList->Clear();
	for (size_t i = 0; i < tempList.size(); i++)
	{
		m_adapterList->Append(tempList[i].friendly_name());
	}
}

void MainFrame::m_updateOnButtonClick(wxCommandEvent& event)
{
	m_updateAdapterList();
}

#ifdef WIN32

/*
 * Copyright (c) 1999 - 2005 NetGroup, Politecnico di Torino (Italy)
 * Copyright (c) 2005 - 2006 CACE Technologies, Davis (California)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Politecnico di Torino, CACE Technologies
 * nor the names of its contributors may be used to endorse or promote
 * products derived from this software without specific prior written
 * permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include <tchar.h>

BOOL LoadNpcapDlls()   // взято из примера npcap, без этого метода monitor mode не работает.
{
	_TCHAR npcap_dir[512];
	UINT len;
	len = GetSystemDirectory(npcap_dir, 480);
	if (!len) {
		fprintf(stderr, "Error in GetSystemDirectory: %x", GetLastError());
		return FALSE;
	}
	_tcscat_s(npcap_dir, 512, _T("\\Npcap"));
	if (SetDllDirectory(npcap_dir) == 0) {
		fprintf(stderr, "Error in SetDllDirectory: %x", GetLastError());
		return FALSE;
	}
	return TRUE;
}
#endif
