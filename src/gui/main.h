﻿///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Oct 26 2018)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#pragma once

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>
#include <wx/choice.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/button.h>
#include <wx/richtext/richtextctrl.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class MainFrame
///////////////////////////////////////////////////////////////////////////////
class MainFrame : public wxFrame
{
private:

protected:
	wxStaticText* m_staticText1;
	wxTextCtrl* m_retrPort;
	wxStaticText* m_staticText2;
	wxTextCtrl* m_filtrPort;
	wxCheckBox* m_logEnable;
	wxStaticText* m_staticText3;
	wxChoice* m_adapterList;
	wxButton* m_update;
	wxButton* m_start;
	wxButton* m_stop;
	wxRichTextCtrl* m_logs;

	bool m_runThread = false;
	bool m_stopThread = false;
	bool m_exit = false;

	// Virtual event handlers, overide them in your derived class
	virtual void MainFrameOnClose(wxCloseEvent& event);
	virtual void MainFrameOnHibernate(wxActivateEvent& event) { event.Skip(); } // TODO
	virtual void m_updateOnButtonClick(wxCommandEvent& event);
	virtual void m_startOnButtonClick(wxCommandEvent& event);
	virtual void m_stopOnButtonClick(wxCommandEvent& event);

	void m_updateAdapterList();


public:

	MainFrame(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Test Wireless Sniffer RTP"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize(619, 585), long style = wxDEFAULT_FRAME_STYLE | wxTAB_TRAVERSAL);

	~MainFrame();

};

