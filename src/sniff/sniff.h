﻿#pragma once

#include <wx/richtext/richtextctrl.h>
#include <wx/socket.h>

#include <tins/tins.h>

class wifi_mon
{
public:
	void run(Tins::Sniffer& sniffer, wxRichTextCtrl* _logs, wxDatagramSocket* _sock2tx, std::string _retrPort, bool _logActive, bool* _stopThread);
private:
	wxIPV4address m_raddr;

	wxRichTextCtrl* m_logs;
	wxDatagramSocket* sock2tx;
	std::string m_retrPort;
	bool m_logActive;
	bool* m_stopThread;
};