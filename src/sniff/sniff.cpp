﻿#include "sniff.h"

#include <wx/app.h>

void wifi_mon::run(Tins::Sniffer& sniffer, wxRichTextCtrl* _logs, wxDatagramSocket* _sock2tx, std::string _retrPort, bool _logActive, bool* _stopThread)
{
	m_logs = _logs;
	sock2tx = _sock2tx;
	m_retrPort = _retrPort;
	m_logActive = _logActive;
	m_stopThread = _stopThread;

	m_raddr.Hostname("127.0.0.1");
	m_raddr.Service(m_retrPort);

	while (*m_stopThread == false)
	{
		wxApp::GetInstance()->Yield();

		Tins::PDU *initial = sniffer.next_packet();

		wxApp::GetInstance()->Yield();

		const Tins::UDP* findUDP = initial->find_pdu<Tins::UDP>();
		if (findUDP != nullptr)
		{
			if (m_logActive == true)
			{
				m_logs->AppendText("[INFO] | Detect UDP | Source port: " + std::to_string(findUDP->sport()) + " | Dest. port: " + std::to_string(findUDP->dport()) + "\n");
			}

			// Extract the RawPDU object.
			const Tins::RawPDU& rawUDP = findUDP->rfind_pdu<Tins::RawPDU>();

			// Finally, take the payload (this is a vector<uint8_t>)
			const Tins::RawPDU::payload_type& payloadUDP = rawUDP.payload();

			if (sock2tx->SendTo(m_raddr, &payloadUDP[0], payloadUDP.size()).LastCount() != payloadUDP.size())
			{
				//throw std::runtime_error("SendDataError");
			}

			if (m_logActive == true)
			{
				m_logs->ScrollIntoView(m_logs->FindNextWordPosition(), WXK_PAGEDOWN);
				m_logs->Update();
			}
		}

		delete initial;
	}
}