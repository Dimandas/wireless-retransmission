﻿#include "app.h"
#include "gui/main.h"

using namespace DetectPositioningWiFi;

wxIMPLEMENT_APP(ProjectApp);

#include <wx/display.h>

bool ProjectApp::OnInit()
{
	wxRect tempObject = wxDisplay((unsigned int)0).GetClientArea();
	tempObject.width = tempObject.width * 0.50;
	tempObject.height = tempObject.height * 0.65;

	MainFrame *FrameMain = new MainFrame(0, wxID_ANY, wxT("Wireless RTP"), wxDefaultPosition, wxSize(tempObject.width, tempObject.height));
	FrameMain->Show(true);

	return true;
}